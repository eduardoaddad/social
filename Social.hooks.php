<?php
/**
 * Curse Inc.
 * Social
 * Social Hooks
 *
 * @author		Brent Copeland
 * @copyright	(c) 2013 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Social
 * @link		https://gitlab.com/hydrawiki
 *
 **/

class SocialHooks {
	/**
	 * Mediawiki OutputPage Instance
	 *
	 * @var		object
	 */
	public static $output;

	/**
	 * Mediawiki $wgScriptPath
	 *
	 * @var		string
	 */
	public static $wgScriptPath;

	/**
	 * Social Settings
	 *
	 * @var		array
	 */
	private static $socialSettings = [];

	/**
	 * Hooks Initialized
	 *
	 * @var		boolean
	 */
	private static $initialized = false;

	/**
	 * Templates for social tag services.
	 *
	 * @var		array
	 */
	private static $socialTagTemplates = [
		'twitter'		=> '<a href="https://twitter.com/share" class="twitter-share-button" data-url="%1$s">Tweet</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\'://platform.twitter.com/widgets.js\';fjs.parentNode.insertBefore(js,fjs);}}(document, \'script\', \'twitter-wjs\');</script>',
		'facebook'		=> '<iframe src="//www.facebook.com/plugins/like.php?href=%1$s&amp;layout=button_count&amp;show_faces=false&amp;width=125&amp;action=like&amp;colorscheme=light&amp;height=20" scrolling="no" frameborder="0"  style="width:125px; height: 20px;"></iframe>',
		'pinterest'		=> '<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script><a href="//www.pinterest.com/pin/create/button/?url=%1$s" data-pin-do="buttonPin" data-pin-config="beside"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" /></a>',
		'reddit'		=> '<a href="//www.reddit.com/submit?url=%1$s"><img src="//www.reddit.com/static/spreddit7.gif" alt="Submit to Reddit"/></a>',
		'stumpleupon'	=> '<script type="text/javascript">(function(){var e=document.createElement("script");e.type="text/javascript";e.async=true;e.src=("https:"==document.location.protocol?"https:":"http:")+"//platform.stumbleupon.com/1/widgets.js";var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)})()</script><su:badge layout="2"></su:badge>',
		'vk'			=> '<a href="http://vk.com/share.php?url=%1$s" target="_blank">Share in VK</a>'
	];

	/**
	 * Default set of social tag services.
	 *
	 * @var		array
	 */
	private static $socialTagServiceDefaults = ['facebook', 'reddit', 'twitter', 'vk'];

	/**
	 * Initiates some needed classes.
	 *
	 * @access	public
	 * @return	void
	 */
	static private function init() {
		if (!self::$initialized) {
			global $wgOut, $wgScriptPath, $wgSocialSettings;

			self::$output		= $wgOut;
			self::$wgScriptPath	= $wgScriptPath;

			self::$socialSettings = $wgSocialSettings;

			self::$initialized = true;
		}
	}

	/**
	 * Sets up this extension's parser functions.
	 *
	 * @access	public
	 * @param	Parser	object
	 * @return	boolean	true
	 */
	static public function onParserFirstCallInit(Parser &$parser) {
		$parser->setHook("social", "SocialHooks::socialTag");

		return true;
	}

	/**
	 * Add resource modules and pop ups.
	 *
	 * @access	public
	 * @param	object	Mediawiki Output Object
	 * @param	object	Mediawiki Skin Object
	 * @return	boolean True
	 */
	static public function onBeforePageDisplay(&$output, &$skin) {
		global $wgUser, $wgSitename;

		// required OpenGraph tags
		$output->addMeta('og:title', $skin->getRelevantTitle());
		$output->addMeta('og:type', 'website');

		$imageNeeded = true;

		$metaTags = $output->getMetaTags();

		foreach ($metaTags as $key => $value) {
			$tagName = $value[0];
			if ($tagName == 'og:image') {
				$imageNeeded = false;
				break;
			}
		}

		$title = $output->getTitle();

		if ($title->getNamespace() == NS_FILE) {
			$mainImage = wfFindFile(Title::newFromText($output->getPageTitle(), NS_FILE));
			if ($mainImage !== false &&
				($mainImage->getMediaType() == MEDIATYPE_BITMAP || $mainImage->getMediaType() == MEDIATYPE_DRAWING)) {
				if (is_object($mainImage)) {
					$ogImage = wfExpandUrl($mainImage->createThumb(1200, 630));
				} else {
					$ogImage = $mainImage;
				}
				$output->addMeta('og:image', $ogImage);
				$imageNeeded = false;
			}
		}

		if ($imageNeeded === true) {
			$output->addMeta('og:image', $skin->getLogo());
		}

		if ($skin->getTitle()) {
			$output->addMeta('og:url', $skin->getTitle()->getCanonicalURL());
		}

		// optional OpenGraph tags
		$output->addMeta('og:site_name', $wgSitename);
		// $output->addMeta('og:description');

		// skip doing the social prompt on mobile views
		if (HydraCore::isMobileSkin($skin)) {
			return true;
		}

		if ((wfTimestamp(TS_UNIX, $wgUser->getRegistration()) + 300) > time()) {
			// Account creation share prompt
			$prompt = new socialPrompt();
			$return = $prompt->load($wgUser, 'onAddNewAccount');

			if ($return) {
				self::init();
				$output->addModuleStyles(['ext.social.styles']);
				$output->addModules(['ext.social.scripts']);
				$misc = $prompt->getMisc();

				self::$output->enableClientCache(false);
				self::$output->addHTML(
					socialPrompt::buildSocialPrompt(
						$prompt->getID(),
						self::$socialSettings['share_messages_after_create_account'],
						wfExpandUrl(self::$wgScriptPath),
						wfMessage('new_account_created')->text(),
						wfMessage('new_account_social')->text(),
						self::$socialSettings['sub_reddit'],
						self::$socialSettings['hash_tags'],
						self::$socialSettings['twitter_via']
					)
				);
			}
		}

		return true;
	}

	/**
	 * Social Side Bar
	 *
	 * @access	public
	 * @param	object	Skin Object
	 * @param	array	Array of bar contents to modify.
	 * @return	boolean True - Must return true or the site will break.
	 */
	static public function onSkinBuildSidebar(Skin $skin, &$bar) {
		$socialSidebarContent = false;

		self::init();
		if (self::$socialSettings['sidebar_enabled'] == true) {
			$skin->getOutput()->addModuleStyles(['ext.social.styles']);
			$skin->getOutput()->addModules(['ext.social.scripts']);
			if (!empty(self::$socialSettings['profile_urls']) && is_array(self::$socialSettings['profile_urls'])) {
				$socialSidebarContent = socialPrompt::buildSocialProfileLinks(self::$socialSettings['profile_urls']);
			}

			$bar["socialProfiles"] = $socialSidebarContent;
		}
		return true;
	}

	/**
	 * Social Sharing Links
	 *
	 * @access	public
	 * @param	object	SkinTemplate Object
	 * @param	array	Array of navigation links to modify.
	 * @return	boolean True - Must return true or the site will break.
	 */
	static public function onSkinTemplateNavigation(SkinTemplate $sktemplate, &$content_navigation) {
		self::init();

		if (self::$socialSettings['navigation_enabled'] == true) {
			$content_navigation['sharing'] = [];
			$content_navigation['sharing']['share'] = [];

			$sktemplate->getOutput()->addModuleStyles(['ext.social.styles']);
			$sktemplate->getOutput()->addModules(['ext.social.scripts']);

			$title = $sktemplate->getTitle();

			$content_navigation['sharing']['share'] = [
				'class' => false,
				'text' => $sktemplate->msg('sharing'),
				'html' => socialPrompt::buildSocialShareLinks(self::$socialSettings['share_messages'], $title->getCanonicalURL(), $title->getFullText(), self::$socialSettings['sub_reddit'], self::$socialSettings['hash_tags'], self::$socialSettings['twitter_via'])
			];
		}

		return true;
	}

	/**
	 * Make prompt flags on ArticleSaveComplete.
	 *
	 * @access	public
	 * @param	object	$article: WikiPage modified
	 * @param	object	$user: User performing the modification
	 * @param	object	$content: MW 1.19, Raw Text, MW 1.21 New content, as a Content object
	 * @param	string	$summary: Edit summary/comment
	 * @param	boolean	$isMinor: Whether or not the edit was marked as minor
	 * @param	boolean	$isWatch: (No longer used)
	 * @param	object	$section: (No longer used)
	 * @param	integer	$flags: Flags passed to WikiPage::doEditContent()
	 * @param	mixed	$revision: New Revision of the article
	 * @param	object	$status: Status object about to be returned by doEditContent()
	 * @param	integer	$baseRevId: the rev ID (or false) this edit was based on
	 * @return	boolean True - Must return true or the site will break.
	 */
	static public function onPageContentSaveComplete($article, $user, $content, $summary, $isMinor, $isWatch, $section, $flags, $revision, $status, $baseRevId) {
		global $wgRequest;

		if ((defined('MW_API') && MW_API === true) ||
			HydraCore::isMobileSkin(RequestContext::getMain()->getSkin())) {
			//We do not want to trigger these prompts from the API or on mobile.
			return true;
		}
		self::init();

		if ($user->getOption('disable_social_prompts') ||
			$wgRequest->getCookie('noSocialPrompts') == 'true' ||
			self::$socialSettings['prompts_enabled'] != true ||
			(self::$socialSettings['prompt_edit_interval'] > 0 && ($user->getEditCount() % self::$socialSettings['prompt_edit_interval']) > 0)) {
			return true;
		}

		$prompt = new socialPrompt();
		$return = $prompt->setUser($user);
		if (!$return) {
			//Do not continue if a valid user was not found.  This is unlikely to ever happen.
			return true;
		}
		$prompt->setHook(__FUNCTION__);

		$title = $article->getTitle();
		$prompt->setMisc(['page' => $title->getFullText(), 'url' => $title->getFullURL()]);

		// Prompt save does return a status, but we are not checking it since we can not interrupt page execution.
		// If we need to check for this in the future use the $prompt->getErrors() function to return errors.
		$success = $prompt->save();

		return true;
	}

	/**
	 * Make prompt flags on ArticleSaveComplete.
	 *
	 * @access	public
	 * @param	obbject	$article: Article the article (object)
	 * @param	boolean	$outputDone: If there is no more output to output, set $outputDone to true.
	 * @param	string	$pcache: If you want the parser cache to try retrieving cached results, set $pcache to done.
	 * @return	boolean True - Must return true or the site will break.
	 */
	static public function onArticleViewHeader(Article &$article, &$outputDone, &$pcache) {
		global $wgUser, $wgRequest;

		if (HydraCore::isMobileSkin(RequestContext::getMain()->getSkin())) {
			return true;
		}

		self::init();

		if ($wgUser->getOption('disable_social_prompts') ||
			$wgRequest->getCookie('noSocialPrompts') == 'true' ||
			self::$socialSettings['prompts_enabled'] != true) {
			return true;
		}

		if($article->getPage()->getUser() == $wgUser->getId() && (wfTimestamp(TS_UNIX, $article->getPage()->getTouched()) + 300) > time()) {
			$prompt = new socialPrompt();
			$return = $prompt->load($wgUser, 'onPageContentSaveComplete');

			if ($return) {
				self::$output->addModuleStyles(['ext.social.styles']);
				self::$output->addModules(['ext.social.scripts']);
				$misc = $prompt->getMisc();

				self::$output->enableClientCache(false);
				self::$output->addHTML(
					socialPrompt::buildSocialPrompt(
						$prompt->getID(),
						self::$socialSettings['share_messages_after_edit'],
						$misc['url'],
						$misc['page'],
						wfMessage('recently_worked_on')->text(),
						self::$socialSettings['sub_reddit'],
						self::$socialSettings['hash_tags'],
						self::$socialSettings['twitter_via']
					)
				);
			}
		}

		return true;
	}

	/**
	 * Triggers a social prompt for new users.
	 *
	 * @access	public
	 * @param	object	Mediawiki User Object
	 * @return	boolean	True
	 */
	static public function onAddNewAccount(User $user, $byEmail) {
		global $wgRequest;

		if (MW_API === true ||
			HydraCore::isMobileSkin(RequestContext::getMain()->getSkin())) {
			//We do not want to trigger these prompts from the API.
			return true;
		}

		self::init();

		if (self::$socialSettings['prompts_enabled'] != true ||
			$user->getOption('disable_social_prompts') ||
			$wgRequest->getCookie('noSocialPrompts') == 'true') {
			return true;
		}

		$prompt = new socialPrompt();
		$return = $prompt->setUser($user);
		if (!$return) {
			//Do not continue if a valid user was not found.  This is unlikely to ever happen.
			return true;
		}
		$prompt->setHook(__FUNCTION__);

		//Prompt save does return a status, but we are not checking it since we can not interrupt page execution.  If we need to check for this in the future use the $prompt->getErrors() function to return errors.
		$success = $prompt->save();

		return true;
	}

	/**
	 * Generates and displays the <social> tag.
	 *
	 * @access	public
	 * @param	string	The input URL between the beginning and ending tags.
	 * @param	array	Array of attribute arguments on that beginning tag.
	 * @param	object	Mediawiki Parser Object
	 * @param	object	Mediawiki PPFrame Object
	 * @return	string	Raw HTML Output
	 */
	static public function socialTag($url, $attributes = array(), Parser $parser, PPFrame $frame) {
		global $wgScriptPath, $wgSitename;

		$urlPrefix = wfExpandUrl($wgScriptPath);

		$services = explode(',', $attributes['services']);
		if (!is_array($services) || !count($services)) {
			$services = self::$socialTagServiceDefaults;
		} else {
			foreach ($services as $key => $service) {
				if (!array_key_exists($service, self::$socialTagTemplates)) {
					unset($services[$key]);
				}
			}
			if (!count($services)) {
				$services = self::$socialTagServiceDefaults;
			}
		}

		$url = filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED);
		if ($url === false) {
			$url = $urlPrefix.$_SERVER['REQUEST_URI'];
		}

		$socialTagOutput = '';
		if ($url !== false && count($services)) {
			foreach ($services as $service) {
				$socialTagOutput .= sprintf(self::$socialTagTemplates[$service], urlencode($url), $urlPrefix);
			}
		}

		if (empty($socialTagOutput)) {
			$socialTagOutput = Xml::span(wfMessage('social_tag_error')->plain(), 'error');
		}

		$parser->getOutput()->addModuleStyles(['ext.social.styles']);
		$parser->getOutput()->addModules(['ext.social.scripts']);

		return $socialTagOutput;
	}

	/**
	 * Setups and Modifies Database Information
	 *
	 * @access	public
	 * @param	DatabaseUpdater	object	[optional]
	 * @return	boolean			true
	 */
	static public function onLoadExtensionSchemaUpdates(DatabaseUpdater $updater = null) {
		$extDir = __DIR__;

		$updater->addExtensionUpdate(array('addTable', 'social_prompt', "{$extDir}/install/sql/social_table_social_prompt.sql", true));

		return true;
	}

	/**
	 * Responds to the preferences page to return new preferences.
	 *
	 * @access	public
	 * @param	User	Object	User Object
	 * @param	array	Preferences Information
	 * @return	boolean	True
	 */
	static function onGetPreferences(User $user, &$defaultPreferences) {
		$defaultPreferences['disable_social_prompts'] = [
			'type'			=> 'toggle',
			'label-message'	=> 'disable_social_prompts',
			'section'		=> 'misc/social'
		];
		return true;
	}
}
