<?php
/**
 * Curse Inc.
 * Dynamic Settings
 * Site Settings Class
 *
 * @author		Alex Smith
 * @copyright	(c) 2013 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Social
 * @link		https://gitlab.com/hydrawiki
 *
**/

class socialPrompt {
	/**
	 * Prompt Data for this instance.
	 *
	 * @var		array
	 */
	protected $prompt = [];

	/**
	 * Prompt Data for this instance.
	 *
	 * @var		array
	 */
	protected $errors = [];

	/**
	 * Loaded from the database?
	 *
	 * @var		boolean
	 */
	protected $loadedFromDatabase = false;

	/**
	 * Constructor
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct() {
		$this->DB = wfGetDB(DB_MASTER);
	}

	/**
	 * Load a Prompt from the Database
	 *
	 * @access	public
	 * @param	object	[Optional] Mediawiki User Object
	 * @param	string	[Optional] Hook name
	 * @return	boolean	Success
	 */
	public function load($user = null, $hook = null) {
		if ($user !== null) {
			$this->setUser($user);
		}
		if (!$this->getUser()) {
			return false;
		}

		if ($hook !== null) {
			$this->setHook($hook);
		}
		if (!$this->getHook()) {
			return false;
		}

		if (User::isIP($this->getUser())) {
			//2016-10-06 Optimizing by removing loading from IP address.
			return false;
		}

		$result = $this->DB->select(
			'social_prompt',
			['*'],
			['user' => $this->getUser(), 'hook' => $this->getHook()],
			__METHOD__
		);

		$prompt = $result->fetchRow();

		if ($prompt['spid'] > 0) {
			$this->prompt = $prompt;
			$this->loadedFromDatabase = true;
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Save a Prompt to the Database
	 *
	 * @access	public
	 * @return	true	Boolean success
	 */
	public function save() {
		if (count($this->errors)) {
			return false;
		}

		if (is_array($this->prompt) && array_key_exists('spid', $this->prompt) && $this->loadedFromDatabase) {
			$this->DB->update(
				'social_prompt',
				$this->prompt,
				['spid' => intval($this->prompt['spid'])],
				__METHOD__
			);
		} else {
			$this->DB->insert(
				'social_prompt',
				$this->prompt,
				__METHOD__
			);
		}

		return true;
	}

	/**
	 * Get errors set by the set functions.
	 *
	 * @access	public
	 * @return	mixed	Array of errors or false for no errors.
	 */
	public function getErrors() {
		return (count($this->errors) ? $this->errors : false);
	}

	/**
	 * Set the User
	 *
	 * @access	public
	 * @param	object	Mediawiki User Object
	 * @return	boolean	True for a valid user or false for an invalid user.
	 */
	public function setUser($user) {
		if (!$user->getId() && User::isIP($user->getName())) {
			$this->prompt['user'] = $user->getName();
			$success = true;
		} elseif ($user->getId()) {
			$this->prompt['user'] = $user->getId();
			$success = true;
		} else {
			$success = false;
		}

		if (!$success) {
			$this->errors['user'] = 'INVALID_USER';
		} else {
			unset($this->errors['user']);
		}

		return $success;
	}

	/**
	 * Set the Hook
	 *
	 * @access	public
	 * @param	string	Hook name
	 * @return	boolean	True for a valid user or false for an invalid hook.
	 */
	public function setHook($hook) {
		if (!empty($hook)) {
			$this->prompt['hook'] = $hook;
			$success = true;
		} else {
			$success = false;
		}

		if (!$success) {
			$this->errors['hook'] = 'BLANK_HOOK';
		} else {
			unset($this->errors['hook']);
		}

		return $success;
	}

	/**
	 * Set the miscellaneous data this prompt needs for display later.
	 * All data will be serialized.
	 *
	 * @access	public
	 * @param	mixed	Data to be serialized and saved.
	 * @return	void
	 */
	public function setMisc($misc) {
		$this->prompt['misc'] = serialize($misc);
	}

	/**
	 * Get the user that initiated this prompt.
	 *
	 * @access	public
	 * @return	mixed	User ID, IP address for a logged out user, or false on failure.
	 */
	public function getUser() {
		if (isset($this->prompt['user']) && (User::isIP($this->prompt['user']) || $this->prompt['user'] > 0)) {
			return $this->prompt['user'];
		}

		return false;
	}

	/**
	 * Get the hook that created this prompt.
	 *
	 * @access	public
	 * @return	mixed	Text string of the hook that created this prompt or false on failure.
	 */
	public function getHook() {
		return (!empty($this->prompt['hook']) ? $this->prompt['hook'] : false);
	}

	/**
	 * Get the miscellaneous data saved for this prompt.
	 *
	 * @access	public
	 * @return	mixed	Unserialized miscellaneous data or false on failure.
	 */
	public function getMisc() {
		return @unserialize($this->prompt['misc']);
	}

	/**
	 * Get the prompt ID saved for this prompt.
	 *
	 * @access	public
	 * @return	mixed	Prompt ID from the database or false for an unsaved prompt.
	 */
	public function getID() {
		return ($this->prompt['spid'] ? $this->prompt['spid'] : false);
	}

	/**
	 * Displays links to social profiles
	 *
	 * @access	public
	 * @param	array	Mapping of service to profile URL
	 * @return	string	Built HTML (may be empty if no URLs are valid)
	 */
	static public function buildSocialProfileLinks($profileUrls) {
		global $wgScriptPath;
		$HTML = '';

		$urlPrefix = wfExpandUrl($wgScriptPath);

		foreach ($profileUrls as $service => $url) {
			$url = filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED);
			if ($url !== false) {
				$linkAttribs = ['href'=>$url, 'target'=>'_blank'];

				$HTML .= "<div class='socialLink $service'>"
					.Html::rawElement(
						'a',
						$linkAttribs,
						Html::element(
							'img',
							[
								'src' => "$urlPrefix/extensions/Social/images/social/$service.svg",
								'width' => '32'
							]
						)
					)."</div>";
			}
		}

		if (!empty($HTML)) {
			$HTML = "<div class='socialSidebar'>
						<script type='text/javascript'>
							document.getElementById('p-socialProfiles').className = 'portal persistent';
						</script>
						".$HTML."
					</div>";
		}
		return $HTML;
	}

	/**
	 * Displays share buttons for social services
	 *
	 * @access	public
	 * @param	array	Array of messages for services to use.
	 * @param	string	URL to the page being shared.
	 * @param	string	Title of page in given URL.
	 * @param	string	[Optional] Reddit subreddit
	 * @param	string	[Optional] Hash Tags for Twitter
	 * @param	string	[Optional] Twitter Via(@Username)
	 * @return	string	Built HTML
	 */
	static public function buildSocialShareLinks($messages = array(), $url, $title, $subReddit = null, $hashTags = null, $twitterVia = null) {
		global $wgScriptPath, $wgSitename;
		$HTML = '';

		$urlPrefix = wfExpandUrl($wgScriptPath);

		foreach (['twitter', 'facebook', 'reddit', 'tumblr'] as $service) {
			$message = isset($messages[$service]) ? $messages[$service] : $messages['default'];
			$message = str_replace(['<wikiname>','<url>','<title>'], [$wgSitename, $url, $title], $message);
			if ($service == 'twitter') {
				$message .= ' --';
			}
			$messages[$service] = urlencode($message);
		}
		$url = urlencode($url);
		$siteName = urlencode($wgSitename);

		$HTML .= '
				<div id="socialIconImages">
					<a href="https://twitter.com/intent/tweet?text='.$messages['twitter'].(!empty($twitterVia) ? '&amp;via='.$twitterVia : null).(!empty($hashTags) ? '&amp;hashtags='.$hashTags : null).'&amp;url='.$url.'" target="_blank"><img src="'.$urlPrefix.'/extensions/Social/images/social/twitter.svg"/></a>'
					.'<a href="https://www.facebook.com/sharer/sharer.php?u='.$url.'" target="_blank"><img src="'.$urlPrefix.'/extensions/Social/images/social/facebook.svg"/></a>'
					.'<a href="https://www.reddit.com/submit?url='.$url.'&amp;title='.$messages['reddit'].(!empty($subReddit) ? '&amp;sr='.$subReddit : null).'" target="_blank"><img src="'.$urlPrefix.'/extensions/Social/images/social/reddit.svg"/></a>'
					.'<a href="https://www.tumblr.com/share/link?url='.$url.'&amp;name='.$siteName.'&amp;description='.$messages['tumblr'].'" target="_blank"><img src="'.$urlPrefix.'/extensions/Social/images/social/tumblr.svg"/></a>'
					.'<a href="https://vk.com/share.php?url='.$url.'" target="_blank"><img src="'.$urlPrefix.'/extensions/Social/images/social/vk.svg"/></a>
				</div>
';

		return $HTML;
	}

	/**
	 * Social Prompt Popup for account created or article edited
	 *
	 * @access	public
	 * @param	integer	Prompt ID
	 * @param	array	Array of messages for services to use.
	 * @param	string	URL to the page being shared.
	 * @param	string	Title of page in given URL.
	 * @param	string	Message text to display at the top.
	 * @param	string	[Optional] Reddit subreddit
	 * @param	string	[Optional] Hash Tags for Twitter
	 * @param	string	[Optional] Twitter Via(@Username)
	 * @return	string	Built HTML
	 */
	static public function buildSocialPrompt($promptID, $messages = array(), $url, $title, $messageText, $subReddit = null, $hashTags = null, $twitterVia = null) {
		$HTML = '
			<div id="socialContainer">
				<div id="socialPrompt">
					<div id="innerPopup">
						<p>'.$messageText.'</p>
						'.self::buildSocialShareLinks($messages, $url, $title, $subReddit, $hashTags, $twitterVia).'
						<div id="socialPopupButtons">
						<a id="promptNeverAgain" class="secondaryButton" href="#">'.wfMessage('never_again').'</a><a id="promptNoThanks" class="secondaryButton" href="#">'.wfMessage('no_thanks').'</a><a id="promptDone" href="#">'.wfMessage('done').'</a>
						</div>
					</div>
				</div>
				<script type="text/javascript">
					window.socialPrompt.ID = '.$promptID.';
				</script>
			</div>
';

		return $HTML;
	}
}
