$(document).ready(function() {
	$('#socialContainer').click(function() {
		hidePrompt();
	});

	$('#socialPrompt').click(function(event) {
		event.stopPropagation();
	});

	$('#promptDone').click(function() {
		hidePrompt();
		$.ajax({
			type: "GET",
			url: mw.config.get('wgScriptPath')+"/api.php",
			data: {action: 'social', spid: window.socialPrompt.ID, do: 'promptDone', format: 'json'}
		});
	});

	$('#promptNeverAgain').click(function() {
		hidePrompt();
		$.ajax({
			type: "GET",
			url: mw.config.get('wgScriptPath')+"/api.php",
			data: {action: 'social', spid: window.socialPrompt.ID, do: 'promptDisable', format: 'json'}
		});
	});

	$('#promptNoThanks').click(function() {
		hidePrompt();
		$.ajax({
			type: "GET",
			url: mw.config.get('wgScriptPath')+"/api.php",
			data: {action: 'social', spid: window.socialPrompt.ID, do: 'promptTimeout', format: 'json'}
		});
	});

	function hidePrompt() {
		$('#socialPrompt').hide();
		$('#socialContainer').fadeOut();
	}
});