<?php
/**
 * Curse Inc.
 * Social
 * Social API
 *
 * @package   Social
 * @author    Alex Smith
 * @copyright (c) 2013 Curse Inc.
 * @license   GPL-2.0-or-later
 * @link      https://gitlab.com/hydrawiki
 **/

class SocialAPI extends ApiBase {
	/**
	 * Social Settings
	 *
	 * @va array
	 */
	private $socialSettings = [];

	/**
	 * Hooks Initialized
	 *
	 * @var boolean
	 */
	private $initialized = false;

	/**
	 * Initiates some needed classes.
	 *
	 * @return void
	 */
	private function init() {
		if (!$this->initialized) {
			global $wgSocialSettings, $wgUser, $wgRequest;

			$this->socialSettings = $wgSocialSettings;
			$this->wgUser         = $wgUser;
			$this->wgRequest      = $wgRequest;

			$this->DB = wfGetDB(DB_MASTER);

			$this->initialized = true;
		}
	}

	/**
	 * Main Executor
	 *
	 * @return void [Outputs to screen]
	 */
	public function execute() {
		$this->init();

		$params = $this->extractRequestParams();

		if (intval($params['spid']) < 1) {
			$this->dieWithError(['invalidspid', $params['spid']]);
		} else {
			$this->spid = intval($params['spid']);
		}

		if ($this->wgUser->getId() < 1 && !User::isIP($this->wgUser->getName())) {
			// This should never get triggered since all logged out users have an IP address.  At least, THEY SHOULD!~
			$this->dieWithError(['invaliduser', $params['do']]);
		}

		if (!$this->wgUser->getId() && User::isIP($this->wgUser->getName())) {
			$this->user = $this->wgUser->getName();
		} elseif ($this->wgUser->getId()) {
			$this->user = $this->wgUser->getId();
		}

		switch ($params['do']) {
			case 'promptDisable':
				$response = $this->promptDisable();
				break;
			case 'promptTimeout':
				$response = $this->promptTimeout();
				break;
			case 'promptDone':
				$response = $this->promptDone();
				break;
			default:
				$this->dieWithError(['invaliddo', $params['do']]);
				break;
		}

		$apiResult = $this->getResult();
		$apiResult->addValue($params['do'], 'success', $response);
	}

	/**
	 * Requirements for API call parameters.
	 *
	 * @return array Merged array of parameter requirements.
	 */
	public function getAllowedParams() {
		return [
			'spid' => [
				ApiBase::PARAM_TYPE     => 'integer',
				ApiBase::PARAM_REQUIRED => true
			],
			'do' => [
				ApiBase::PARAM_TYPE     => 'string',
				ApiBase::PARAM_REQUIRED => true
			]
		];
	}

	/**
	 * Disable Prompts Permanently
	 *
	 * @return boolean Success
	 */
	public function promptDisable() {
		$this->DB->delete('social_prompt', ['user' => $this->user], __METHOD__);
		$total = $this->DB->affectedRows();
		$this->wgRequest->response()->setcookie(
			'noSocialPrompts',
			'true',
			time() + $this->socialSettings['prompt_cookie_time']
		);

		// Change preference for logged in users.
		if ($this->wgUser->getId() > 0) {
			$this->wgUser->setOption('disable_social_prompts', true);
			$this->wgUser->saveSettings();
		}

		if (!$total) {
			return 'false';
		} else {
			return 'true';
		}
	}

	/**
	 * Set a timeout on the prompts to show up later on.
	 *
	 * @return boolean Success
	 */
	public function promptTimeout() {
		$this->DB->delete('social_prompt', ['user' => $this->user, 'spid' => $this->spid], __METHOD__);
		$total = $this->DB->affectedRows();
		$this->wgRequest->response()->setcookie(
			'noSocialPrompts',
			'true',
			time() + $this->socialSettings['prompt_cookie_time']
		);

		if (!$total) {
			return 'false';
		} else {
			return 'true';
		}
	}

	/**
	 * Remove the existing prompt entry.
	 *
	 * @return boolean Success
	 */
	public function promptDone() {
		$this->DB->delete('social_prompt', ['user' => $this->user, 'spid' => $this->spid], __METHOD__);
		$total = $this->DB->affectedRows();

		Hooks::run('CompletedSocialAction', [$this->wgUser]);

		if (!$total) {
			return 'false';
		} else {
			return 'true';
		}
	}

	/**
	 * Get version of this API Extension.
	 *
	 * @return string API Extension Version
	 */
	public function getVersion() {
		return '1.0';
	}
}
