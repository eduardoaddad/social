CREATE TABLE /*_*/social_prompt (
  `spid` int(14) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `misc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`spid`),
  KEY `user_hook` (`user`,`hook`)
) /*$wgDBTableOptions*/;